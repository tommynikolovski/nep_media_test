import React from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import LoginPage from './LoginPage';
import App from './App';

const Router = () =>(
    <BrowserRouter>
      <Switch>
        <Route path="/" component={App} exact/>
        <Route path="/loginpage" component={LoginPage}/>
      </Switch>
    </BrowserRouter>
);

export default Router;