import React, {Component} from 'react';
import { Link } from "react-router-dom";
import LoginPage from './LoginPage';

class LandingPage extends Component {
    render(){
      return (
        <div>
          <header>
            <a id="loginLink" href="#">Log In</a>
          </header>
          <LoginPage />
        </div>
    
      );
    }
}

export default LandingPage;